package main

import (
	"context"
	"fmt"
	"io"
	"io/fs"
	"os"
	"parallel-enc/base"
	"path"

	"github.com/sirupsen/logrus"
)

func walkDirReading(ctx context.Context, ch chan<- base.Request, chroot fs.FS, from string, ignoreDotfiles bool) error {
	log := logrus.WithFields(logrus.Fields{
		"function": "walkDir",
		"dir":      from,
	})

	if err := fs.WalkDir(chroot, from, func(pth string, entry fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if ignoreDotfiles && path.Base(pth)[0] == '.' {
			if entry.IsDir() {
				return fs.SkipDir // Ignore the whole dir
			}

			return nil // Just ignore the file
		}

		if err := ctx.Err(); err != nil {
			return fmt.Errorf("walking %s: %w", pth, err)
		}

		info, err := entry.Info()
		if err != nil {
			return fmt.Errorf("get %s info: %w", pth, err)
		}

		var linkTarget string
		if info.Mode()&os.ModeSymlink == os.ModeSymlink {
			linkTarget, _ = os.Readlink(pth)
		}

		req := base.Request{
			Path:       pth,
			Info:       info,
			LinkTarget: linkTarget,
			Chan:       make(chan io.ReadCloser),
		}

		log.Debugf("Send req for %s", req.Path)

		select {
		case ch <- req:
		case <-ctx.Done():
			return fmt.Errorf("send request for %s: %w", pth, ctx.Err())
		}

		return nil
	}); err != nil {
		return fmt.Errorf("walk dir %s: %w", from, err)
	}

	return nil
}
