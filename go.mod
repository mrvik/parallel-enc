module parallel-enc

go 1.17

require (
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20211124211545-fe61309f8881 // indirect
)
