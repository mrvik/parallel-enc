package base

import (
	"io"
	"io/fs"
)

type Request struct {
	// Path that has been read. Absolute path from the FS root.
	Path string
	// Link target in case this describes a symlink
	LinkTarget string
	// File info (from stat).
	Info fs.FileInfo
	// Single value channel for the file reader.
	// The file reader will send the file and the archiver will receive it.
	// This channel should be buffered to avoid blocking the reader.
	Chan chan io.ReadCloser
}
