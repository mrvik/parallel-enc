//go:build linux || darwin || freebsd || openbsd

package main

import (
	"os"

	"golang.org/x/sys/unix"
)

func signals() []os.Signal {
	return []os.Signal{
		unix.SIGINT,
		unix.SIGTERM,
		unix.SIGPIPE,
	}
}
