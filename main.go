package main

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"parallel-enc/base"
	"runtime"

	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
)

func main() {
	cwd, _ := os.Getwd()
	workersFlag := flag.Int("workers", runtime.NumCPU(), "Num of read workers")
	chrootFlag := flag.String("chroot", cwd, "Chroot into path")
	logLevel := flag.Int("loglevel", int(logrus.WarnLevel), "Verbosity level (0-6)")
	ignoreDotfiles := flag.Bool("ignore-dotfiles", false, "Ignore dotfiles on list")
	color := flag.Bool("color", false, "Force color")

	flag.Parse()
	logrus.SetLevel(logrus.Level(*logLevel))
	logrus.SetOutput(os.Stderr)
	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors: *color,
	})

	log := logrus.WithFields(logrus.Fields{
		"package": "main",
		"workers": *workersFlag,
		"chroot":  *chrootFlag,
	})

	ctx, cancel := signal.NotifyContext(context.Background(), signals()...)
	defer cancel()

	var (
		workers     = *workersFlag
		chroot      = os.DirFS(*chrootFlag)
		directories = flag.Args()
		pendingChan = make(chan base.Request, workers*2)
		doneChan    = make(chan base.Request, len(pendingChan))
		output      = os.Stdout

		actx, acancel     = context.WithCancel(ctx)
		reader, readerctx = errgroup.WithContext(actx)
		dirListing, dlctx = errgroup.WithContext(readerctx)
	)

	if workers <= 0 {
		logrus.Fatal("Workers cannot be <= 0")
	}

	log.Debug("Generating directory list routines")

	for _, dir := range directories {
		directory := dir

		dirListing.Go(func() error {
			return walkDirReading(dlctx, pendingChan, chroot, directory, *ignoreDotfiles)
		})
	}

	log.Debug("Generating reader routines")

	for i := 0; i < workers; i++ {
		reader.Go(func() error {
			return readerWorker(readerctx, chroot, pendingChan, doneChan)
		})
	}

	log.Debug("Starting archiver routine")

	archiverErr := make(chan error, 1)
	go func() {
		archiverErr <- createArchiver(ctx, doneChan, output, acancel)
	}()

	log.Debug("Waiting for directory listing to finish")

	if err := dirListing.Wait(); err != nil {
		log.Errorf("Error listing directories: %s", err)
	}

	close(pendingChan)

	log.Debug("Waiting for file reader to finish")

	if err := reader.Wait(); err != nil {
		log.Errorf("File reader error: %s", err)
	}

	close(doneChan)

	log.Info("Waiting for archiver to finish")

	<-actx.Done()

	if err := <-archiverErr; err != nil {
		log.Errorf("Archiver error: %s", err)
	}

	if err := ctx.Err(); err != nil {
		log.Fatalf("Global context error: %s", err)
	}
}
