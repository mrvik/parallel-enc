package main

import (
	"archive/tar"
	"context"
	"fmt"
	"io"
	"parallel-enc/base"

	"github.com/sirupsen/logrus"
)

func createArchiver(ctx context.Context, rchan <-chan base.Request, w io.Writer, endFn func()) error {
	defer endFn()

	archive := tar.NewWriter(w)
	defer archive.Close()

	for {
		var work base.Request

		select {
		case <-ctx.Done():
			return fmt.Errorf("receive on archiver: %w", ctx.Err())
		case w, ok := <-rchan:
			if !ok {
				return nil // Done
			}

			work = w
		}

		if err := handleWrite(archive, work); err != nil {
			return err
		}
	}
}

func handleWrite(into *tar.Writer, work base.Request) error {
	reader := <-work.Chan
	if reader != nil {
		defer reader.Close()
	}

	logrus.WithFields(logrus.Fields{
		"size": work.Info.Size(),
		"mode": work.Info.Mode(),
	}).Infof("Store %s", work.Path)

	header, err := tar.FileInfoHeader(work.Info, work.LinkTarget)
	if err != nil {
		return fmt.Errorf("generate tar header for %s: %w", work.Path, err)
	}

	if work.Info.IsDir() && work.Path[len(work.Path)-1] != '/' {
		work.Path += "/"
	}

	header.Name = work.Path

	if err := into.WriteHeader(header); err != nil {
		return fmt.Errorf("write tar header for %s: %w", work.Path, err)
	}

	if !work.Info.Mode().IsRegular() || reader == nil {
		return nil
	}

	defer reader.Close()

	var buf [4096]byte

	if _, err := io.CopyBuffer(into, reader, buf[:]); err != nil {
		return fmt.Errorf("copy %s into archive: %w", work.Path, err)
	}

	return nil
}
