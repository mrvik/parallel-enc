package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"io/fs"
	"parallel-enc/base"

	"github.com/sirupsen/logrus"
)

type BufferedReadCloser struct {
	*bufio.Reader
	closer io.Closer
}

func NewBufferedReadCloser(rc io.ReadCloser) BufferedReadCloser {
	return BufferedReadCloser{
		bufio.NewReader(rc),
		rc,
	}
}

func (brc BufferedReadCloser) Close() error {
	if err := brc.closer.Close(); err != nil {
		return fmt.Errorf("buffered reader closer: %w", err)
	}

	return nil
}

func readerWorker(ctx context.Context, root fs.FS, rchan <-chan base.Request, schan chan<- base.Request) error {
	log := logrus.WithFields(logrus.Fields{
		"function": "reader",
	})

	for {
		var work base.Request

		select {
		case <-ctx.Done():
			return fmt.Errorf("reader receive: %w", ctx.Err())
		case w, ok := <-rchan:
			if !ok {
				return nil // Done
			}

			work = w
		}

		log.Debugf("Opening file %s", work.Path)

		file, err := root.Open(work.Path)
		if err != nil {
			return fmt.Errorf("open file %s to read: %w", work.Path, err)
		}

		work.Chan = make(chan io.ReadCloser, 1)
		work.Chan <- NewBufferedReadCloser(file)
		close(work.Chan)

		log.Debugf("Sending file %s", work.Path)

		select {
		case <-ctx.Done():
			return fmt.Errorf("reader send: %w", ctx.Err())
		case schan <- work:
		}
	}
}
